﻿//! Copyright (c) 2014, Mads Klinkby All rights reserved.
/*jslint browser: true, plusplus: true, regexp: true, white: true */

(function (define, require) {
    "use strict";

    define(["ng", "moment", "ngResource"], function (ng, moment) {

        return ng.module("app", ["ngResource"])
            .config(['$httpProvider', function($httpProvider) {
                $httpProvider.defaults.headers.common.Accept = "application/json;odata=verbose";
            }])
            .factory("FeedProxy", ["$resource", function ($resource) {
                return $resource("https://query.yahooapis.com/v1/public/yql", null,
                    {
                        get: {
                            method: "JSONP",
                            callback: "JSON_CALLBACK",
                            params: {
                                format: "json",
                                callback: "JSON_CALLBACK"
                            }
                        }
                    });
            }])
            .filter('fromNow', function() {
                    return function(dateString) {
                        return moment(dateString).fromNow();
                    };
                })
                .controller("FeedReaderCtrl", ["$scope", "FeedProxy", "$window", function ($scope, $feedProxy, $window) {
                    var settings = { maxentries: 10 };
                location.href.replace(
                    /([^?=&]+)(=([^&]*))?/g,
                    function ($0, $1, $2, $3) { settings[$1.toLowerCase()] = decodeURIComponent($3); }
                );

                $scope.showTitle = "true" === settings.showtitle;
                $scope.loaded = false;
                $scope.proxy = null;
                /*
                    "query":{
                        "count":10,
                        "created":"2017-01-17T08:03:11Z",
                        "lang":"en-US",
                        "results":{
                                "item":[{
                                    "title": "Intent to deprecate and remove: JavaScript",
                                    "link": "http://feedproxy.google.com/~r/ajaxian/~3/vJWugBU_28s/intent-to-deprecate-and-remove-javascript", "comments": ["http://ajaxian.com/archives/intent-to-deprecate-and-remove-javascript#respond", "0"], "pubDate": "Wed, 01 Apr 2015 14:40:24 +0000", "creator": "Michael Mahemoff", "category": "Front Page", "guid": { "isPermaLink": "false", "content": "http://ajaxian.com/?p=10888" }, "description": "Blink has been a frequent source of innovation ever since it forked and a bold proposal on the Blink list today is no exception. We&#8217;re all about deprecation and removal around here, so Konstantin Nikitin&#8217;s idea created quite a buzz at Ajaxian HQ. Removing JavaScript will lead to significant performance improvements: — A lot of <a href=\"http:
                */

                function refreshFeed() {
                    $scope.proxy = $feedProxy.get({
                        q: "select * from rss where url='"
                        + settings.feedurl.replace("'", "`")
                        + "' limit "
                        + settings.maxentries
                    }, function () {
                        $scope.loaded = true;
                        $window.setTimeout(function () {
                            var el = $window.document.body;
                            if (settings.sphosturl) {
                                $window.parent.postMessage("<message senderId=" + settings.senderid + ">resize(" + el.offsetWidth + ", " + el.offsetHeight + ")</message>", settings.sphosturl);
                            }
                        }, 50);
                    });
                }
                refreshFeed();
            }])
        ;
    });

}(window.define, window.require));
