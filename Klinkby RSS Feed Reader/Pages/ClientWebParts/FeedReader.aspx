﻿<%@ Page language="C#" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<WebPartPages:AllowFraming ID="AllowFraming" runat="server" />

<html>
<head>
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../../Content/App.css" />
    <script type="text/javascript">
        'use strict';

        // Set the style of the client web part page to be consistent with the host web.
        (function () {
            var hostUrl = '';
            if (document.URL.indexOf('?') != -1) {
                var params = document.URL.split('?')[1].split('&');
                for (var i = 0; i < params.length; i++) {
                    var p = decodeURIComponent(params[i]);
                    if (/^SPHostUrl=/i.test(p)) {
                        hostUrl = p.split('=')[1];
                        document.write('<link rel="stylesheet" href="' + hostUrl + '/_layouts/15/defaultcss.ashx" />');
                        break;
                    }
                }
            }
            if (hostUrl == '') {
                document.write('<link rel="stylesheet" href="/_layouts/15/1033/styles/themable/corev15.css" />');
            }
        })();
    </script>
</head>
<body>
    <div data-ng-controller="FeedReaderCtrl">
        <div data-ng-hide="loaded">
            <i class="icon-spinner"></i> Loading...
        </div>
        
        <div class="feed">
            <!--       
			<h3 data-ng-show="showTitle">
			    <a class="feed-title ms-heroCommandLink" target="_top" 
                    data-ng-href="{{proxy.query.link}}" 
                    data-ng-title="{{proxy.query.description}}">
			        <span data-ng-bind="proxy.responseData.feed.title"></span>       
			    </a>
			</h3>
            -->
            <ul class="feed-list ms-linksection-listRoot">
                <li data-ng-repeat="item in proxy.query.results.item" class="feed-entry ms-linksection-listItem ng-cloak">
                    <div class="feed-entry-header">
                        <a href data-ng-href="{{item.origLink || item.link}}" target="_top">
                            <span data-ng-bind="item.title" class="feed-title ms-linksection-title"></span>
                        </a>
                        <span class="feed-meta">
                            <span class="feed-date ms-soften" data-ng-bind="item.pubDate | fromNow"></span>
                        </span>
                    </div>
                    <div data-ng-bind="item.encoded" class="feed-entry-text ms-descriptiontext"></div>
                </li>
            </ul>
        </div>
    </div>

    <script type="text/javascript" src="../../Scripts/lib/require/2.1.10/require.min.js" data-main="../../Scripts/main.js" async></script>
</body>
</html>
